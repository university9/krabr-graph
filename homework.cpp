/*
 * Instituto Tecnologico de Cartago
 * Analisis de Algoritmos
 * TareaHPC
 * Estudiantes:
 *          David Salazar
 *          Arturo Vasquez
 *          Hussein Smith
 *          Juan Carlos Ruiz
 * Primer Semestre 2018
 */

#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <set>
#include <fstream>
#include "timer.h"
#include <cstdlib>
#include <vector>
#include <unordered_set>
#include <omp.h>
#include <iostream>
#include <fstream>
using namespace std;



void writeLine(string texto, string fileName)
{
    ofstream out_file(fileName, ios::app);
    out_file << texto;
    out_file.close();
}

void createFile(string file)
{
    //auto myfile = fstream(file, std::ios::out | std::ios::binary);
    ofstream myfile(file);
    myfile << "";
    myfile.close();
}
//Concatena dos unordered_set<int>
unordered_set<int> getUnion(unordered_set<int>& a, unordered_set<int>& b)
{
  unordered_set<int> result = a;
  result.insert(b.begin(), b.end());
  return result;
}
//Sirve para observar el comportamiento, de los vectores utilizados, mas no se utiliza en ejecuci�n.
void print_vector(vector<int>* path)
{
    for (vector<int>::iterator it = path->begin(); it != path->end(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
}
//v2s= vectorToSet, se encarga de pasar de un vector a un set
unordered_set<int> v2s(vector<int>* path)
{
    unordered_set<int> obj;
    for (vector<int>::iterator  it = path->begin(); it != path->end(); ++it)
    {
        obj.insert(*it);
    }
    return obj;
}
//Construye parejas a partir de un vector ingresado, por decirlo de otra manera, todos los posibles resultados en C(n,2)
vector<pair<int,int>> make_pairs(vector<int>* path)
{
    vector<pair<int,int>> pairs;
    int repetitions = path->size();
    int cont = 0;

    for (vector<int>::iterator  it = path->begin(); it != path->end(); ++it)
    {

        for (int i = 1; i < repetitions; i++)
        {
             #pragma omp critical
             {
                pairs.push_back(make_pair(*it, path->at(i + cont)));
             }
        }
        repetitions--;
        cont++;
    }
    return pairs;
}

struct GraphNode
{
    int id;
    int weight;

    int next;
    int *nextNodes;

    int last;
    int *lastNodes;
};
//Class Metric: Se utiliza para almacenar los resultados obtenidos en ejcuci�n
// y realizar algunos calculos para obtener la informacion faltante.
class Metric
{
    public:

        int absolute;
        unordered_set<int> net;
        int total;

        Metric(int absolute, unordered_set<int> net,int total){ this->absolute = absolute; this->net = net; this->total = total;}
        Metric(){this->absolute = 0; this->total = 0;}
        //Obtiene la suma neta
        int getNet(map<int,GraphNode> graph)
        {
            int net_sum = 0;
            for (  unordered_set<int>::iterator it = net.begin(); it != net.end(); ++it)
            {
                net_sum = net_sum + graph[(*it)].weight;
            }
            return net_sum;
        }

        float getPromedy()
        {
            if (this->total != 0)
                {return float(this->absolute)/float(this->total);}
            else
                {return 0;}
        }
};
//Combina las estructuras de datos utilizadas para mantenerlas en una sola variable.
struct Metrics
{
    map<int,Metric*> fan_in;
    map<int,Metric*> fan_out;
    map<pair<int,int>,Metric*> pairs;
};
// Interpreta e imprime el grafo leido, no es necesario para realizar los calculos.
void showGraph(map<int,GraphNode>& graph)
{
    for(map<int,GraphNode>::iterator it = graph.begin(); it != graph.end(); ++it)
    {
        cout << "NODO: " << it->first << endl;
        cout << "PESO: " << it->second.weight << endl;
        cout << "NODOS SIGUIENTES: " <<it->second.next << ", ESTOS SON: " << endl;
        for (int i = 0; i < it->second.next; i++)
        {
            cout << it->second.nextNodes[i] << " ";
        }
        cout << endl;
        cout << "NODOS ANTERIORES: " <<it->second.last << ", ESTOS SON: " << endl;
        for (int i = 0; i < it->second.last; i++){
            cout << it->second.lastNodes[i] << " ";
        }
        cout << endl << endl;
    }
}
//Lee el grafo directamente de un archivo.
void getGraph(map<int,GraphNode>& graph,string filename)
{
    ifstream file;
    file.open(filename);
    if (file.is_open())
    {
        int num;
        while(!file.eof())
        {
            GraphNode node;

            file >> num;
            node.id = num;

            file >> num;
            node.weight = num;

            file >> num;
            node.next = num;
            node.nextNodes = new int[num];

            for (int i = 0; i < node.next; i++)
            {
                file >> num;
                node.nextNodes[i] = num;
            }

            file >> num;
            node.last = num;
            node.lastNodes = new int[num];

            for (int i = 0; i < node.last; i++)
            {
                file >> num;
                node.lastNodes[i] = num;
            }

            graph[node.id] = node;
        }
    }
}
// Como su nombre le indica, calcula el FAN_OUT de manera recursiva, para un unico nodo.
//Por otro lado crea el flujo de nodos con programaci�n dinamica. Esto para reutilizar recursos en ejuci�n.
void getFAN_OUT(map<int,GraphNode> graph, int node, int& parcial, Metrics& metrics, int father, vector<int>* path)
{
    if (graph[node].next == 0)
        return;

    int tparcial = parcial;


    for (int i = 0; i < graph[node].next; i++)
    {
        #pragma omp critical
        {
        path->push_back(graph[node].nextNodes[i]);
        metrics.fan_out[father]->net.insert(node);
        metrics.fan_out[father]->net.insert(graph[node].nextNodes[i]);


        parcial = parcial + graph[graph[node].nextNodes[i]].weight;

        //print_vector(path);

        metrics.fan_out[father]->absolute = metrics.fan_out[father]->absolute + parcial;
        metrics.fan_out[father]->total++;
        }


        vector<pair<int,int>> pairs = make_pairs(path);


        for (vector<pair<int,int>>::iterator it = pairs.begin(); it != pairs.end(); ++it)
        {
            if (metrics.pairs.count(*it) == 1)
            {
                #pragma omp critical
                {
                metrics.pairs[*it]->absolute = metrics.pairs[*it]->absolute + parcial;
                unordered_set<int> spath = v2s(path);
                metrics.pairs[*it]->net = getUnion(metrics.pairs[*it]->net, spath);
                metrics.pairs[*it]->total++;
                }
            }
            else
            {
                #pragma omp critical
                {
                Metric* metric = new Metric(parcial,v2s(path),1);
                metrics.pairs[*it] = metric;
                }
            }
        }

        getFAN_OUT(graph, graph[node].nextNodes[i], parcial, metrics, father, path);

        parcial = tparcial;
        #pragma omp critical
        {
        path->pop_back();
        }
    }
}
// A diferencia del getFAN_OUT, este algoritmo solo calcula el FAN_IN.
void getFAN_IN(map<int,GraphNode> graph, int node, int& parcial, Metrics metrics, int father)
{
    if (graph[node].last == 0)
        return;

    int tparcial = parcial;

    for (int i = 0; i < graph[node].last; i++)
    {
        #pragma omp critical
        {
        metrics.fan_in[father]->net.insert(node);
        metrics.fan_in[father]->absolute = metrics.fan_in[father]->absolute + parcial;
        metrics.fan_in[father]->total++;
        }

        getFAN_IN(graph, graph[node].lastNodes[i], parcial, metrics, father);

        parcial = tparcial;
    }
}
// Debido a ciertos errores de compilaci�n se opto por crear una nueva funcion, en
// vez de utilizar to_String.
string toString(int number){
    string result = "";
    result = static_cast<std::ostringstream*>(&(std::ostringstream() << number))->str();
    return result;
}
string toString(float number){
    string result = "";
    result = static_cast<std::ostringstream*>(&(std::ostringstream() << number))->str();
    return result;
}
//Fuci�n principal para el calculoo de todas las metricas, esta funci�n se decidio
// paralelizar ya que en algoritmo pasa mucho tiempo dentro de esta, lo que ayudaria
// a un mejor rendimiento.
bool getMetrics(map<int,GraphNode>& graph)
{

    Metrics metrics;
    int i=0;
    createFile("In-Out.txt");
    createFile("Pairs.txt");
    string data="";
    string data2="";
    #pragma omp parallel for private(i) shared(graph,metrics,data)
    for( i=0;i<graph.size();i++){

        map<int,GraphNode>::iterator it=graph.begin();
        advance(it,i);

        #pragma omp parallel sections
        {
            #pragma omp section
            {
                #pragma omp critical
                {
                    metrics.fan_out[(it->second).id] = new Metric();
                }
                vector<int> path;
                path.push_back((it->second).id);
                getFAN_OUT(graph, (it->second).id, (it->second).weight, metrics, (it->second).id, &path);
            }
             #pragma omp section
            {

                #pragma omp critical
                {
                    metrics.fan_in[(it->second).id] = new Metric();
                }
                metrics.fan_in[(it->second).id]->total = 0;
                getFAN_IN(graph, (it->second).id, (it->second).weight, metrics, (it->second).id);
            }
        }

        string id = toString((it->second).id);
        string abs = toString(metrics.fan_out[(it->second).id]->absolute);
        string net = toString(metrics.fan_out[(it->second).id]->getNet(graph));
        string prom = toString(metrics.fan_out[(it->second).id]->getPromedy());

        string  abs2 = toString(metrics.fan_in[(it->second).id]->absolute);
        string  net2 = toString(metrics.fan_in[(it->second).id]->getNet(graph));
        string  prom2 = toString(metrics.fan_in[(it->second).id]->getPromedy());

        #pragma omp critical
        {
            data = data + " " + id + " " + net2  + " " + abs2 + " " + prom2+ " " + net + " " + abs + " " + prom + "\n";
        }


        }
	int j=0;
        #pragma omp parallel for private(j) shared(graph,metrics,data2)
        for(int j=0;j<metrics.pairs.size();j++){
            map<pair<int,int>,Metric*>::iterator et=metrics.pairs.begin();
            advance(et,j);
            string id1 = toString((et->first).first);
            string id2 = toString((et->first).second);
            string abs = toString((et->second)->absolute);
            string net = toString((et->second)->getNet(graph));
            string prom = toString((et->second)->getPromedy());
            #pragma omp critical
            {
                data2 = data2 + id1 +" "+ id2+ " " + net + " " + abs + " " + prom + "\n";
            }
        }


        writeLine(data2,"Pairs.txt");

        writeLine(data,"In-Out.txt");

    return true;
}
//Sirve para observar los resultados de las metricas. De una manera mas clara
// Al igual que ShowGraph y ShowVector, no es necesario para realizar calculos.
void showMetrics(Metrics& metrics, map<int,GraphNode> graph)
{
    cout << "FAN OUT" << endl << endl;

    for(map<int,Metric*>::iterator it = metrics.fan_out.begin(); it != metrics.fan_out.end(); ++it)
    {
        cout << "NODO: " << (it->first) << endl;

        cout << "TOTAL: " << (it->second)->total << endl;
        cout << "ABSOLUTO: " << (it->second)->absolute << endl;
        cout << "NETO: " << (it->second)->getNet(graph) << endl;
        cout << "PROMEDIO: " << (it->second)->getPromedy() << endl;
        cout << endl;
    }

    cout << "FAN IN" << endl << endl;

    for(map<int,Metric*>::iterator it = metrics.fan_in.begin(); it != metrics.fan_in.end(); ++it){
        cout << "PROMEDIO: " << (it->second)->getPromedy() << endl;
        cout << endl;
    }

    cout << "PARES DE NODOS" << endl << endl;

    for (map<pair<int,int>,Metric*>::iterator  it = metrics.pairs.begin(); it != metrics.pairs.end(); ++it)
    {
        cout << "NODO 1: " << (it->first).first << ", NODO 2: " << (it->first).second << endl << endl;
        cout << "TOTAL: " << (it->second)->total << endl;
        cout << "ABSOLUTO: " << (it->second)->absolute << endl;
        cout << "NETO: " << (it->second)->getNet(graph) << endl;
        cout << "PROMEDIO: " << (it->second)->getPromedy() << endl << endl;
    }
}


int main(int argc, char *argv[])
{

    string filename="";
    if(argc>1){
	filename=argv[1];
	} else{
	     cout<<"Usage: "<<argv[0]<<" <filename>"<<endl;
	     exit(1);
	}

    timerStart();
    double duration =0;

    map<int,GraphNode> graph;
    getGraph(graph,filename);
    bool result = getMetrics(graph);
	duration = timerStop();
	cout << "Execution time: " << duration << " seconds" << endl;

    return 0;
}














